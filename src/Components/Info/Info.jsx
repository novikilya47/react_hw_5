import React, {useState, useContext} from 'react';
import Context from '../../utils/context';
import Input from './Input'

function Info(props) {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [cart, setCart] = useState("");
    const context = useContext(Context);

    const handleEdit = () => {
        let user = {
            name: name ? name : context.userState.name,
            email: email ? email : context.userState.email,
            cart: cart ? cart : context.userState.cart
        };
        context.editUser(user);
    }
    
    return (
        <div className="Info">
            <div>Имя: {context.userState.name}</div>
            <div>Email: {context.userState.email}</div>
            <div>Номер карты: {context.userState.cart}</div>
            <div className="Info_inputs">
                <Input data = {name} setInputData = {setName} text={"Имя"}/>
                <Input data = {email} setInputData = {setEmail} text={"Email"}/>
                <Input data = {cart} setInputData = {setCart} text={"Номер карты"} flag = {true}/>
            </div>  
            <button onClick={() => handleEdit()}>Изменить информацию</button>   
        </div>
    )
}

export default Info;
