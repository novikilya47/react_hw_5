import React from 'react'; 
import HandleError from './HandleError.jsx'

 function Input(props) {
    return (
        <> 
        <p>{props.text}</p>
        <div className="Input_obert">
            <div className="Info_input"><textarea value={props.data} type="text" onChange={(e) => props.setInputData(e.target.value)} /></div>
            {props.flag && <HandleError length = {props.data.length}/>}
        </div>
        </>
    )  
}

export default React.memo(Input);