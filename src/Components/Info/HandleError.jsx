import React from 'react'; 
const  HandleError = (props) => {

    console.log(props.length)

    const message = function render(){
        if (props.length < 1) {
            return ''
        } else if ( props.length > 14 && props.length < 16 ) {
            return 'Недостаточное количество символов' 
        } else if ( props.length === 16 ) { 
            return <p>&#10003;</p>
        } else if ( props.length > 16 && props.length < 18 )
            return 'Слишком большое количество символов'
    }
  
    return (
      <div className="Message">
        <p>{message()}</p>
      </div>
    )
  }
  
  export default React.memo(HandleError, (prevProps, nextProps)=>{
    if (nextProps.length > 14 && nextProps.length < 18) {
      return false
    } else if (prevProps.length > 14 && prevProps.length < 18) {
      return false
    } return true
  })