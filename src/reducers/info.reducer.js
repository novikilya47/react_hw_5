import {EDIT_USER} from "../Constants/constants";
export const defaultUserState = {name: "Ilya", email : "novikilya@mail.ru", cart : "1111 3333 4444 5555"}

export function infoReducer (state = defaultUserState, action){
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                email: action.data.email,
                cart: action.data.cart,
            }
            return newState;
        default:
            return state;
    }
};